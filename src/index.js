import cookie from 'cookie';
import jwt from 'jsonwebtoken';

const { JsonWebTokenError } = jwt;

/**
 * Class for reading objects from HTTP requests and
 * saving objects to responses, using json web tokens under the hood
 */
class JwtCookie {
	#secret;
	#cookieKey;

	/**
	 * Create an instance of JwtCookie
	 * @param {string} secret The secret to use to generate and verify the web tokens
	 * @param {string} cookieKey The key to use for the cookie, defaults to `session`
	 */
	constructor(secret, cookieKey='session'){
		this.#secret = secret;
		this.#cookieKey = cookieKey;
	}

	/**
	 * Get the object from the request. Returns null if none is set.
	 * @param {http.IncomingMessage} req The incoming request object
	 */
	get(req) {
		const c = req.headers.cookie;
		if(!c) {
			return null;
		}
		const cookies = cookie.parse(c);
		const token = cookies[this.#cookieKey];

		try {
			const decoded = token && jwt.verify(token, this.#secret);
			return decoded;
		} catch(e) {
			// Return null if token is malformed
			if(e instanceof JsonWebTokenError && e.message=='jwt malformed') {
				return null;
			}

			throw e;
		}
	}

	/**
	 * Write an object to the response's cookie header
	 * @param {http.ServerResponse} res The response object to write to
	 * @param {*} payload The object to write to the response cookie
	 */
	set(res, payload) {
		const token = jwt.sign(payload, this.#secret);
		const c = cookie.serialize(this.#cookieKey, token, { httpOnly: true });
		res.setHeader('Set-Cookie', c);
	}
}

export default JwtCookie;
