
<a name="JwtCookie"></a>

## JwtCookie
Class for reading objects from HTTP requests and
saving objects to responses, using json web tokens under the hood



* [JwtCookie](#JwtCookie)
    * [new JwtCookie(secret, cookieKey)](#new_JwtCookie_new)
    * [.get(req)](#JwtCookie+get)
    * [.set(res, payload)](#JwtCookie+set)

<a name="new_JwtCookie_new"></a>

### new JwtCookie(secret, cookieKey)
Create an instance of JwtCookie


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| secret | <code>string</code> |  | The secret to use to generate and verify the web tokens |
| cookieKey | <code>string</code> | <code>&quot;session&quot;</code> | The key to use for the cookie, defaults to `session` |

<a name="JwtCookie+get"></a>

### jwtCookie.get(req)
Get the object from the request. Returns null if none is set.

**Kind**: instance method of [<code>JwtCookie</code>](#JwtCookie)

| Param | Type | Description |
| --- | --- | --- |
| req | <code>http.IncomingMessage</code> | The incoming request object |

<a name="JwtCookie+set"></a>

### jwtCookie.set(res, payload)
Write an object to the response's cookie header

**Kind**: instance method of [<code>JwtCookie</code>](#JwtCookie)

| Param | Type | Description |
| --- | --- | --- |
| res | <code>http.ServerResponse</code> | The response object to write to |
| payload | <code>\*</code> | The object to write to the response cookie |

