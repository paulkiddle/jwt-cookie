# JWT Cookie

Small library for reading and writing objects to http requests and responses.

## Usage

```bash
npm install jwt-cookie
```

```javascript
import JwtCookie from 'jwt-cookie';

const jwtc = new JwtCookie(
	process.env.JWT_SECRET, // A secret string to use for signing the JWT
	'my_session' // The cookie key to use (optional)
);

export default function requestHandler(req, res) {
	const session = jwt.get(req) || { count: 0 };

	session.count++;

	jwt.set(res, session);

	res.end(`You've viewed this page ${session.count} times.`)
}
```

## API
