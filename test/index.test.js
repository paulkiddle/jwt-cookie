import JwtCookie from '../src/index.js';
import { jest } from '@jest/globals';

test('JWTCookie', () => {
	const j = new JwtCookie('sec', 'ses');

	const res = {
		setHeader: jest.fn((n,c)=>res.cookie=c)
	};

	j.set(res, {data: 'my-data'});

	expect(res.setHeader).toHaveBeenCalledWith('Set-Cookie', expect.stringMatching(/^ses=/));

	const req = {
		headers: {
			cookie: res.cookie.split(';')[0]
		}
	};

	expect(j.get(req)).toEqual(expect.objectContaining({data:'my-data'}));

});

test('blank', ()=>{
	const j = new JwtCookie('h');
	const req = {
		headers: {
			cookie: null
		}
	};


	expect(j.get(req)).toEqual(null);
});


test('Returns null if cookie is malformed', ()=>{
	const j = new JwtCookie('h');
	const req = {
		headers: {
			cookie: 'session=malformed'
		}
	};


	expect(j.get(req)).toEqual(null);
});



test('Throws if cookie is expired', ()=>{
	const expired = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjEsInBheWxvYWQiOiJwYXlsb2FkIiwiaWF0IjoxNjIyMTI4NDkyfQ.XOKPgZctH-zdAID2vThk_aUBfGGv7p-RLeUL4mlaI9w';
	const j = new JwtCookie('h');
	const req = {
		headers: {
			cookie: 'session='+expired
		}
	};

	expect(()=>j.get(req)).toThrow(Error);
});
